#include <stdio.h>
#include <stdexcept>

void myExceptions() {
    printf("MY EXCEPTIONS\n");
    throw std::logic_error("bla\n");
    // program terminates here because of "noexcept"
    printf("MY EXCEPTIONS - exit\n");
}

void myNoExceptions() noexcept {
    printf("my no exceptions - enter\n");
    myExceptions();
    printf("my no exceptions - before exit\n");
}

int main() {
    printf("start\n");
    try {
        myNoExceptions();
    }
    catch(...) {
        printf("recovered");
    } 
    printf("end\n");
}