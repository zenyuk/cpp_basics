#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    char* c = malloc(100);
    printf("before\n");
    memcpy(c, "123456789", 10);
    printf("result: %s\n", c);
    free(c);
    printf("result 2: %s\n", c);
    free(c);
    printf("result 3: %s\n", c); // not printed
    printf("end\n");
}