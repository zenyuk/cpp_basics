#include <stdio.h>

void
receiveNotIncrementedCopy(int x) {
    printf("got: %d\n", x);
}

int
main() {
    int i = 5;
    receiveNotIncrementedCopy(i++);
    printf("%d  \ndone", i);
}