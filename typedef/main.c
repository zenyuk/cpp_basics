#include <stdio.h>

struct S {
    int a;
};

typedef struct P { // P in this line not required
    int a;
} P;

typedef int I;

int main(){

    I i = 4;
    #define x 5;
    // in C (not C++) have to write "struct S"
    struct S s;
    s.a = 1;
    
    P p;
    p.a = 2;;

    #ifdef x
    printf("\n ha: %d", i);
    #endif
}