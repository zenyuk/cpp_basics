#include <stdio.h>

// must be before include assert.h
#define NDEBUG
#include <assert.h>

int main()
{
    int x = 1;
    assert(x == 2);
    puts("after assert");
}