#include <stdio.h>
#include <string>

struct My {
    std::string s;
    int x;
};

int
main() {
    My m;
    printf("res: %d \n", m.x);

    return 0;
}