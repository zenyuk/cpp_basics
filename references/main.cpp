#include <string>
#include "aclass.h"

using namespace anamespace;

// compile with:
// g++ main.cpp aclass.cpp -o mytest
int main()
{
  std::string name = "t6";
  printf("Hello, %s !\n", name.c_str());

  aclass* my = new aclass();
  my->amount = 5;
  my->amethod();

  aclass &myr = *my;
  aclass myc = *my;

  my->amount = 7;
  printf("result: \n");
  myr.amethod();
  myc.amethod();


  delete my;
  return 0;
}
