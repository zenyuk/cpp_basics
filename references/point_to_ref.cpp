#include <stdio.h>

class My {
    int* x;
public:
    My() {
        x = new int(11);
    }
    int& getX() {
        return *x;
    }
    void setX() {
        x = nullptr;
    }
};

int
main(){
    My m;
    printf("%d\n", m.getX());
    int& y = m.getX();
    y = 3;
    printf("%d\n", m.getX());
    m.setX();
    printf("%d\n", y);
}