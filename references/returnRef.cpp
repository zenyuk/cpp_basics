#include <stdio.h>

//warning: reference to stack memory associated with local variable 'x' returned
int& bad() {
    int x;

    return x;
}

int main() {
    int y = 4;
    y = bad();
    printf("result: %d", y);
}