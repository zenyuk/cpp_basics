#include <stdio.h>

struct X {
    int x;
};

class My {
    X x;
public:
    // returns a copy of X
    X Get() {
        return x;
    }
    void Set(X x) {
        My::x = x;
    }
};

int main() {
    X x;
    x.x = 5;

    My m;
    m.Set(x);

    X x2;
    x2 = m.Get();
    x2.x = 4;

    printf("result: %d", m.Get().x);
}