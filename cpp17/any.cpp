#include <any>
#include <vector>

int main()
{
    std::vector<std::any> v{1, 5.5, "bla"};
    int x = 4;
    printf("1st type: %s\n", v[0].type().name());
    printf("1st: %d", std::any_cast<int>(v[0]));
}