#include <stdio.h>
#include <string>

int
main() {
    char* s = "abc";

    s = nullptr;

    if (s) {
        std::string str = std::string(s);
        puts("before\n");
        printf("out: %s", str.c_str());
    }

    //puts(s);
    puts("end");
}