#include <stdio.h>
#include <string.h>

int main() {
    // will cause segmentation fault - ready only memory
    //char* c = "abc5\0";
    char c[] = "abc5\0";

    *(c + 1) = 'X';

    for (int i=0; i<strlen(c); ++i) {
        char* cp = c + i;
        printf("%c", *cp);
    }
}