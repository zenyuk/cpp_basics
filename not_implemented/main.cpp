#include <stdio.h>

class My {
public:
    My(int x) {
        size = x;
    }
    // implementation is missing
    void GetSize();
private:
    int size;
};

int main() {
    My m(7);
    // will build with below commented out
    //m.GetSize();
    printf("bla: %d", 5);
}