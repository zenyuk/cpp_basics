#include <iostream>
#include <string>

int main()
{
  int i[3] = {1,3,4};
  int* pi = &i[0];
  pi++;
  std::cout << "Hello, " << *pi << "!\n";
}