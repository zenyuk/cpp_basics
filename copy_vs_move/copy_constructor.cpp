#include <stdio.h>
#include <string>

struct S {
    S() = default;
    S(const S& s) = delete;

    int x;
    int y;
    std::string s;
};

int main() {
    S s{3, 4, "abc"};
    // won't compile
    S s1(s); 
    printf("x: %d\ny: %d\ns: %s\n", s1.x, s1.y, s1.s.c_str());
    s.x = 5;

}