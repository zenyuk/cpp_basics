#include <stdio.h>

class My {
public:
    My() {}
    
    int getX() {
        return x;
    };
private:
    int x;
};

int
main() {
    My m;
    printf("x: %d", m.getX());
    return 0;
}